# Wordinator  #

### What the hell is this? ###

* Wordinator is a small application for generating words from letters.
* The application relies on aspell for word checking.
* the general syntax is 'wordinator [letters]' with an optional switch to change the language.
* Make sure you have aspell and an appropriate aspell dictionary installed before using this program!

### How to install! ###

* The application installation follows the standard make, make install procedure.
* The only depedency is aspell, which should be installed on most newer linux systems.