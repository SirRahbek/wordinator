#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <aspell.h>
#include <string.h>

using namespace std;

AspellConfig*       spell_config  = 0;
AspellCanHaveError* possible_err  = 0;
AspellSpeller*      spell_checker = 0;

string lang      = "";
string letters   = "";
int    matches   = 0;
int    low_limit = 4;

vector<string> words;


void pickAndPass (string build, string rest) {
	if (build.length() >= low_limit) {
		int correct = aspell_speller_check(spell_checker, build.c_str(), -1);	
		if (correct) {
			words.push_back(build); } }

	for (int n=0 ; n<rest.length() ; ++n) {
		string newRest = "";
		for (int m=0 ; m<rest.length() ; ++m) {
			if (m!=n) 
				newRest += rest[m]; }
		pickAndPass(build+rest[n], newRest); }

	return; }


void get_config (int argc, char* argv[]) {
	for (int n=1 ; n<argc ; ++n) {
		string arg = argv[n];

		if (arg == "-l") {
			++n;
			if (argc > n)
				lang = argv[n]; }
		else {
			letters += arg; } }
	return; }


int main (int argc, char* argv[]) {
	if (argc < 2) {
		cout << "Wordinator 0.02" << endl;
		cout << "Usage: "<< endl;
		cout << "  Input a string of letters, to see all possible words they can produce." << endl;
		cout << "  Special characters are not yet supported." << endl; }

	get_config(argc, argv);

	spell_config  = new_aspell_config();
	if (lang != "") {
		aspell_config_replace(spell_config, "lang", lang.c_str() );
		printf( "Set language to %s\n" , lang.c_str() ); }

	possible_err  = new_aspell_speller(spell_config);
	spell_checker = to_aspell_speller(possible_err);

	if (letters.length() > 7) {	
		long fac = 1;
		for (int n=1 ; n<=letters.length() ; n++ )
			fac *= n;

		printf("\nSearch list consists of %d letters,\n", letters.length());
		printf("which means %d possible words in total!\n", fac );
		printf("This can lead to a long search time.\n");
		printf("Are you sure you want to continue? [y/n]\n");
		
		char choice[32];
		scanf("%s", choice);
		if ( !strcmp(choice, "y")==0  && !strcmp(choice, "Y")==0 ) {
			return 1; } }

	pickAndPass("", letters);
	sort(words.begin(), words.end());

	printf("Searching in [%s]\n", letters.c_str());
	printf("======= Word List =======\n");
	for (string word : words)
		printf("  %s\n", word.c_str());
	printf("========== End ==========\n");
	printf("Found %d matches\n", words.size());

	delete_aspell_config(spell_config);
	return 0; }
